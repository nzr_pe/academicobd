DROP DATABASE IF EXISTS academicobd;

CREATE DATABASE academicobd CHARACTER SET utf8mb4;

USE academicobd;

CREATE TABLE profesores (

        idprofesor INT NOT NULL AUTO_INCREMENT,
        nombre VARCHAR(60) NOT NULL,
        apellidos VARCHAR(60) NOT NULL,
        usuario VARCHAR (50) NOT NULL,
        contrasena VARCHAR(50) NOT NULL,
        estado VARCHAR(2) NOT NULL,
        foto VARCHAR(100) NOT NULL,

        PRIMARY KEY (idprofesor)

)ENGINE=InnoDb;


CREATE TABLE alumnos(

        idalumno INT NOT NULL AUTO_INCREMENT,
        nombre VARCHAR (60) NOT NULL,
        apellidos VARCHAR (60) NOT NULL,
        usuarioalumno VARCHAR (60) NOT NULL,
        contrasena VARCHAR (60) NOT NULL,
        fecha_nacimiento DATE NOT NULL,
        estado VARCHAR (2) NOT NULL,
        foto VARCHAR(100) NOT NULL,

        PRIMARY KEY (idalumno)
)ENGINE=InnoDb;            


CREATE TABLE cursos (

        idcurso INT NOT NULL AUTO_INCREMENT,
        nombre VARCHAR (50) NOT NULL,
        estado VARCHAR(2) NOT NULL,
        idprofesor INT NOT NULL,

        PRIMARY KEY (idcurso),
        FOREIGN KEY(idprofesor) REFERENCES profesores(idprofesor)

)ENGINE=InnoDb;

CREATE TABLE fichas_cursos (

        idficha_curso INT NOT NULL AUTO_INCREMENT,
        fechainscripcion DATE NOT NULL,
        idalumno INT NOT NULL,
        idcurso INT NOT NULL,

        PRIMARY KEY (idficha_curso),
        FOREIGN KEY (idalumno) REFERENCES alumnos (idalumno),
        FOREIGN KEY (idcurso) REFERENCES  cursos (idcurso)
)ENGINE=InnoDb;

CREATE TABLE notas(

        idnota INT NOT NULL AUTO_INCREMENT,
        nota   INT NOT NULL,
        idficha_curso INT NOT NULL,

        PRIMARY KEY (idnota),
        FOREIGN KEY (idficha_curso) REFERENCES fichas_cursos (idficha_curso)

)ENGINE=InnoDb;

-- inserto datos en tabla profesores

INSERT INTO `profesores` (`idprofesor`, `nombre`, `apellidos`, `usuario`, `contrasena`, `estado`, `foto`) VALUES (NULL, 'Rosa Elvira', 'Mayta Tupac', 'RosaTupac', '123', '1', 'sdfsdfsdfsdf334456');
INSERT INTO `profesores` (`idprofesor`, `nombre`, `apellidos`, `usuario`, `contrasena`, `estado`, `foto`) VALUES (NULL, 'Renato', 'Ugarte Machaca', 'UgarteMachaca', '123', '1', 'sdfsdfsdfsdf334456');
INSERT INTO `profesores` (`idprofesor`, `nombre`, `apellidos`, `usuario`, `contrasena`, `estado`, `foto`) VALUES (NULL, 'Carlos Enrrique', 'Mcgregor Janampa', 'CarlosMcjanampa', '123', '1', 'sdfsdfsdfsdf334456');
INSERT INTO `profesores` (`idprofesor`, `nombre`, `apellidos`, `usuario`, `contrasena`, `estado`, `foto`) VALUES (NULL, 'Martha', 'Condori Ramirez', 'MarthaCondori', '123', '1', 'sdfsdfsdfsdf334456');
INSERT INTO `profesores` (`idprofesor`, `nombre`, `apellidos`, `usuario`, `contrasena`, `estado`, `foto`) VALUES (NULL, 'Miriam', 'Cahuana Saenz', 'MiriCaSa', '123', '1', 'sdfsdfsdfsdfdsds34456');
INSERT INTO `profesores` (`idprofesor`, `nombre`, `apellidos`, `usuario`, `contrasena`, `estado`, `foto`) VALUES (NULL, 'Alexis', 'Cabrera Sotomayor', 'CabreSoto', '123', '1', 'sdfsdsdsdsdfsdf334322ds456');
INSERT INTO `profesores` (`idprofesor`, `nombre`, `apellidos`, `usuario`, `contrasena`, `estado`, `foto`) VALUES (NULL, 'Hellen', 'Maximo Carhuas', 'HellenMaca', '123', '1', 'sdfsdfsdssddfsdf334456');
INSERT INTO `profesores` (`idprofesor`, `nombre`, `apellidos`, `usuario`, `contrasena`, `estado`, `foto`) VALUES (NULL, 'Ursula', 'Letona Carrazco', 'UrsulaLeto', '123', '0', 'sdfsdfsdfsdf334dsd3w456');
INSERT INTO `profesores` (`idprofesor`, `nombre`, `apellidos`, `usuario`, `contrasena`, `estado`, `foto`) VALUES (NULL, 'Pedro Ramon', 'Caranca Monzon', 'Ramoncaranca', '123', '1', 'sdfsdfs232355dfsdf334dsd3w456');
INSERT INTO `profesores` (`idprofesor`, `nombre`, `apellidos`, `usuario`, `contrasena`, `estado`, `foto`) VALUES (NULL, 'Cirilo Dante', 'Aranda Collana', 'CiriloDante', '123', '0', 'sdfsdfsdsdsddfsdf334dsd3w456');

-- inserto datos en tabla alumnos 

INSERT INTO `alumnos` (`idalumno`, `nombre`, `apellidos`, `usuarioalumno`, `contrasena`, `fecha_nacimiento`, `estado`, `foto`) VALUES (NULL, 'Junior', 'Ballesteros Gamarra', 'Jubaga','123', '1999-01-11', '1', 'asdashdasldahsldjaskdas/as//**asdasd');
INSERT INTO `alumnos` (`idalumno`, `nombre`, `apellidos`, `usuarioalumno`, `contrasena`, `fecha_nacimiento`, `estado`, `foto`) VALUES (NULL, 'Ramiro', 'Allesta Altamirano', 'Raalal', '123', '2002-03-01', '1', 'asdashdaddsd/sldahsldjaskdas/as//**asdasd');
INSERT INTO `alumnos` (`idalumno`, `nombre`, `apellidos`, `usuarioalumno`, `contrasena`, `fecha_nacimiento`, `estado`, `foto`) VALUES (NULL, 'Priscila Stefani', 'Ancca Raimondi', 'PriAnra', '123', '1998-04-10', '1', 'asdashdasldahsldjaskdas/as//**asdasd');
INSERT INTO `alumnos` (`idalumno`, `nombre`, `apellidos`, `usuarioalumno`, `contrasena`, `fecha_nacimiento`, `estado`, `foto`) VALUES (NULL, 'Anderson Joel', 'Fernades Aquino', 'AnFeAq', '123', '2003-08-02', '1', 'asdashdasldahsld2jaskdas/as//**asdasd');
INSERT INTO `alumnos` (`idalumno`, `nombre`, `apellidos`, `usuarioalumno`, `contrasena`, `fecha_nacimiento`, `estado`, `foto`) VALUES (NULL, 'Patricia', 'Ramua Choque', 'Paracho', '123', '2002-09-12', '1', 'asdashd2aslda2hsldjaskdas/as//**asdasd');
INSERT INTO `alumnos` (`idalumno`, `nombre`, `apellidos`, `usuarioalumno`, `contrasena`, `fecha_nacimiento`, `estado`, `foto`) VALUES (NULL, 'Manuel', ' Gamarra Pereira', 'Magape', '123', '1999-07-09', '1', 'asdashdasldahs2qldja2skdas1/as//**asdasd');
INSERT INTO `alumnos` (`idalumno`, `nombre`, `apellidos`, `usuarioalumno`, `contrasena`, `fecha_nacimiento`, `estado`, `foto`) VALUES (NULL, 'Jennifer', 'Romero Ansaldi', 'Jeroan', '123', '2003-09-09', '1', 'asdashdasldahs2qldja2skdas1/as//**asdasd');
INSERT INTO `alumnos` (`idalumno`, `nombre`, `apellidos`, `usuarioalumno`, `contrasena`, `fecha_nacimiento`, `estado`, `foto`) VALUES (NULL, 'Hans Rene', 'Sotelo Vivaldi', 'Hansovi', '123', '2001-01-05', '1', 'asd//ashdasldahs2qldja2skdas1/as//**asdasd');
INSERT INTO `alumnos` (`idalumno`, `nombre`, `apellidos`, `usuarioalumno`, `contrasena`, `fecha_nacimiento`, `estado`, `foto`) VALUES (NULL, 'Peter', 'Mallma Dalma', 'Pemada', '123', '2001-01-02', '1', 'asdashd878/asldahs2qldja2skdas1/as//**asdasd');
INSERT INTO `alumnos` (`idalumno`, `nombre`, `apellidos`, `usuarioalumno`, `contrasena`, `fecha_nacimiento`, `estado`, `foto`) VALUES (NULL, 'Cristal', 'Montecinos Manrrique', 'CriMon', '123', '2004-08-01', '1', 'asdashd2878/asldahs2qldja2skdas1/as//**asdasd');
INSERT INTO `alumnos` (`idalumno`, `nombre`, `apellidos`, `usuarioalumno`, `contrasena`, `fecha_nacimiento`, `estado`, `foto`) VALUES (NULL, 'Andrea', 'Callens Figueroa', 'Ancafi', '123', '2003-07-06', '1', 'asdashd878/asldahs2qldja2skdas1/as//**asdasd');
INSERT INTO `alumnos` (`idalumno`, `nombre`, `apellidos`, `usuarioalumno`, `contrasena`, `fecha_nacimiento`, `estado`, `foto`) VALUES (NULL, 'Danny', 'Pajoy Rangel', 'Dapara', '123', '2005-07-06', '1', 'asdashd878/asldahs2qldja//2skdas1/as//**asdasd');
INSERT INTO `alumnos` (`idalumno`, `nombre`, `apellidos`, `usuarioalumno`, `contrasena`, `fecha_nacimiento`, `estado`, `foto`) VALUES (NULL, 'Randy', 'Ojeda Farfan', 'Ancafi', '123', '2002-11-16', '1', 'asdashd878/asldah//s2qldja2skdas1/as//**asdasd');

-- inserto datos en la tabla Cursos

INSERT INTO `cursos` (`idcurso`, `nombre`, `estado`, `idprofesor`) VALUES (NULL, 'Matematica Avanzada', '1', '1');
INSERT INTO `cursos` (`idcurso`, `nombre`, `estado`, `idprofesor`) VALUES (NULL, 'Computacion', '1', '6');
INSERT INTO `cursos` (`idcurso`, `nombre`, `estado`, `idprofesor`) VALUES (NULL, 'Ingles', '1', '4');
INSERT INTO `cursos` (`idcurso`, `nombre`, `estado`, `idprofesor`) VALUES (NULL, 'Frances', '1', '2');
INSERT INTO `cursos` (`idcurso`, `nombre`, `estado`, `idprofesor`) VALUES (NULL, 'Literatura', '1', '5');

-- inserto datos en la tabla Fichas_Cursos
INSERT INTO `fichas_cursos` (`idficha_curso`, `fechainscripcion`, `idalumno`, `idcurso`) VALUES (NULL, '2021-02-02', '9', '2');
INSERT INTO `fichas_cursos` (`idficha_curso`, `fechainscripcion`, `idalumno`, `idcurso`) VALUES (NULL, '2021-02-01', '3', '1');
INSERT INTO `fichas_cursos` (`idficha_curso`, `fechainscripcion`, `idalumno`, `idcurso`) VALUES (NULL, '2021-01-20', '4', '3');
INSERT INTO `fichas_cursos` (`idficha_curso`, `fechainscripcion`, `idalumno`, `idcurso`) VALUES (NULL, '2021-01-25', '2', '1');

-- inserto datos a la tabla Notas
INSERT INTO `notas` (`idnota`, `nota`, `idficha_curso`) VALUES (NULL, '15', '1');
INSERT INTO `notas` (`idnota`, `nota`, `idficha_curso`) VALUES (NULL, '10', '2');
INSERT INTO `notas` (`idnota`, `nota`, `idficha_curso`) VALUES (NULL, '11', '3');
INSERT INTO `notas` (`idnota`, `nota`, `idficha_curso`) VALUES (NULL, '18', '4');
